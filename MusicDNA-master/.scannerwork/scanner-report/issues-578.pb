w
javaS3973GUse indentation to denote the code conditionally executed by this "if". 2
�� :
�
�� y
javaS3973IUse indentation to denote the code conditionally executed by this "else". 2
�� 	:
�
�� !w
javaS3973GUse indentation to denote the code conditionally executed by this "if". 2
�� .:
�
�� y
javaS3973IUse indentation to denote the code conditionally executed by this "else". 2
�� 	:
�
�� 'J
javaS3740/Provide the parametrized type for this generic. 2ZZ L
javaS3740/Provide the parametrized type for this generic. 2
�� L
javaS3740/Provide the parametrized type for this generic. 2
�� L
javaS3740/Provide the parametrized type for this generic. 2
�� L
javaS3740/Provide the parametrized type for this generic. 2
�� L
javaS3740/Provide the parametrized type for this generic. 2
�� L
javaS3740/Provide the parametrized type for this generic. 2
�� L
javaS3740/Provide the parametrized type for this generic. 2
�� L
javaS3740/Provide the parametrized type for this generic. 2
�	�	 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 L
javaS3740/Provide the parametrized type for this generic. 2
�
�
 �
javaS1192QDefine a constant instead of duplicating this literal "argument is null" 5 times. )      @2
��( ::
�
��( :Duplication:
�
��( :Duplication:
�
��( :Duplication:
�
��( :Duplication:
�
��% 7Duplication�
javaS1192UDefine a constant instead of duplicating this literal "node has no children" 3 times. )      @2
��. D:
�
��. DDuplication:
�
��& <Duplication:
�
��& <Duplication�
javaS1192QDefine a constant instead of duplicating this literal "No more elements" 4 times. )      @2RR# 5:
�RR# 5Duplication:
�
��' 9Duplication:
�
��' 9Duplication:
�
��# 5Duplicationb
javaS112FDefine and throw a dedicated exception instead of using a generic one. 2
�� b
javaS112FDefine and throw a dedicated exception instead of using a generic one. 2
�� b
javaS112FDefine and throw a dedicated exception instead of using a generic one. 2
�� b
javaS112FDefine and throw a dedicated exception instead of using a generic one. 2
�� b
javaS112FDefine and throw a dedicated exception instead of using a generic one. 2
�� b
javaS112FDefine and throw a dedicated exception instead of using a generic one. 2
�	�	 b
javaS112FDefine and throw a dedicated exception instead of using a generic one. 2
�	�	 [
javaS1155>Use isEmpty() to check whether the collection is empty or not. 2
�� m
javaS117QRename this local variable to match the regular expression '^[a-z][a-zA-Z0-9]*$'. 2
�� m
javaS117QRename this local variable to match the regular expression '^[a-z][a-zA-Z0-9]*$'. 2
�	�	 o
javaS1104RMake object a static final constant or non-public and provide accessors if needed. 2
�
�
 m
javaS1104PMake next a static final constant or non-public and provide accessors if needed. 2
�
�
 �
javaS25898Remove this expression which always evaluates to "false" 2
�� :]
,�
�� Expression is always false.
-�
��
 Implies 'node1' can be null.�
javaS25897Remove this expression which always evaluates to "true" 2
�� /:�
+�
�� /Expression is always true.
,�
�� Implies 'ancestor' is true.
2�
�� /!Implies 'descendant' is not null.
0�
��	 Implies 'ancestor' is not null.�
javaS1149eReplace the synchronized class "Vector" by an unsynchronized one such as "ArrayList" or "LinkedList". 2ZZ m
javaS1149PReplace the synchronized class "Stack" by an unsynchronized one such as "Deque". 2
�
�
 �
javaS1149eReplace the synchronized class "Vector" by an unsynchronized one such as "ArrayList" or "LinkedList". 2
�
�
 �
javaS1149eReplace the synchronized class "Vector" by an unsynchronized one such as "ArrayList" or "LinkedList". 2
�
�
 m
javaS1149PReplace the synchronized class "Stack" by an unsynchronized one such as "Deque". 2
�� `
javaS1124EReorder the modifiers to comply with the Java Language Specification. 2NN `
javaS1124EReorder the modifiers to comply with the Java Language Specification. 2]] W
javaS1117:Rename "parent" which hides the field declared at line 87. 2
�� A
javaS1659$Declare "level2" on a separate line. 2
�� ?
javaS1659"Declare "diff" on a separate line. 2
�� @
javaS1659#Declare "node2" on a separate line. 2
�� C
javaS1168&Return an empty array instead of null. 2
��	 �
javaS3776RRefactor this method to reduce its Cognitive Complexity from 16 to the 15 allowed. )      �?2
��" -:
�
�� +1:*
(�
�� +2 (incl 1 for nesting):*
(�
�� +3 (incl 2 for nesting):*
(�
�� +4 (incl 3 for nesting):*
(�
�� +4 (incl 3 for nesting):
�
�� +1:
�
�� +19
javaS2225Return empty string instead. 2
�	�	 p
javaS2975SRemove this "clone" implementation; use a copy constructor or copy factory instead. 2
�	�	 f
javaS4201IRemove this unnecessary null check; "instanceof" returns false for nulls. 2
�	�	 J
javaS4508-Make sure deserializing objects is safe here. 2
�
�
 #H
javaS1150+Implement Iterator rather than Enumeration. 2
�
�
/ Dm
javaS1149PReplace the synchronized class "Stack" by an unsynchronized one such as "Deque". 2
�
�
 �
javaS1149eReplace the synchronized class "Vector" by an unsynchronized one such as "ArrayList" or "LinkedList". 2
�
�
 Y
javaS1117<Rename "children" which hides the field declared at line 90. 2
�
�
 H
javaS1150+Implement Iterator rather than Enumeration. 2
�
�
0 EH
javaS1905+Remove this unnecessary cast to "TreeNode". 2
�
�
 H
javaS1150+Implement Iterator rather than Enumeration. 2
�
�
3 H�
javaS1149eReplace the synchronized class "Vector" by an unsynchronized one such as "ArrayList" or "LinkedList". 2
�
�
 Y
javaS1117<Rename "children" which hides the field declared at line 90. 2
�
�
 H
javaS1150+Implement Iterator rather than Enumeration. 2
��7 Lm
javaS1149PReplace the synchronized class "Stack" by an unsynchronized one such as "Deque". 2
�� �
javaS2293�Replace the type specification in this constructor call with the diamond operator ("<>"). (sonar.java.source not set. Assuming 7 or greater.) 2
��  